#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//    printf("Iteradores: FOR\n");
//
//    int upperLimit, bottomLimit;
//
//    printf("Imprimir en orden descendiente \n");
//
//    printf("Ingresa el limite inferior: \n");
//    scanf("%i", &bottomLimit);
//
//    printf("Ingresa el limite superior: \n");
//    scanf("%i", &upperLimit);
//
//    for (int i=upperLimit; i >= bottomLimit; i--) {
//        printf("%i\n", i);
//    }
//
//    return 0;
//}


// reto: secuencia de fibonacci.
int main() {
    int limite, intermedio;
    int primero = 0, segundo = 1;

    printf("Ingresa la cant. de terminos de la secuencia de Fibonacci: \n");
    scanf("%i", &limite);

    for (int i=0; i < limite; i++) {
        if (i == 0)
            printf("0\n");
        else if (i == 1)
            printf("1\n");
        else {
            intermedio = primero + segundo;
            printf("%i\n", intermedio);

            primero = segundo;
            segundo = intermedio;
        }
    }

    return 0;
}
