#include <stdio.h>
#include <stdlib.h>

// primer reto: calcular �rea de un cilindro (radio, altura, area y volumen).
//int main()
//{
//    float radio, altura, volumen, area;
//    float pi = 3.14;
//
//    printf("Ingres� el radio de la base: ");
//    scanf("%f", &radio);
//
//    printf("Ingres� la altura del cilindro: ");
//    scanf("%f", &altura);
//
//    // calculamos el �rea de la base.
//    area = (2 * pi * radio) * (altura + radio);
//
//    // calculamos el volumen del cilindro.
//    volumen = area * altura;
//
//    // imprimimos los valores.
//    printf("El area de la base es: %f\n", area);
//    printf("El volumen del cilindro es: %f\n", volumen);
//
//    return 0;
//}

// segundo reto: pasar de grados fahrenheit a celsius.
int main() {
    float fahrenheit, celsius;

    printf("Hola! Vamos a transformar grados Fahrenheit a grados Celsius.\n");

    printf("Ingres� la temperatura en grados Fahrenheit: \n");
    scanf("%f", &fahrenheit);

    celsius = (fahrenheit - 32) / 1.8;
    printf("La temperatura en grados Celsius es: %f", celsius);

    return 0;
}
