#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//int main()
//{
//    printf("Recorrido de matrices: \n");
//
//    int integerArray[4][5];
//
//    for (int i=0; i < 4; i++) {
//        for (int j=0; j < 5; j++) {
//            integerArray[i][j] = ((i+j) * 100 + j);
//            printf("(%i, %i): %i\n", i, j, integerArray[i][j]);
//        }
//    }
//
//    return 0;
//}

// reto.
int main() {
    float calificaciones[5][6];

    float promedio;
    for (int i=0; i < 5; i++) {
        promedio = 0;
        for (int j=0; j < 5; j++) {
            calificaciones[i][j] = rand() % 5 + 6;
            promedio += calificaciones[i][j];
        }

        promedio /= 5;
        calificaciones[i][5] = promedio;
        printf("El promedio de la fila %i es: %f\n", i, calificaciones[i][5]);
    }

    return 0;
}
