#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Operadores relacionales\n");

    float a, b;
    a = 5;
    b = 10;

    if (a < b)
        printf("condicion 1: VERDADERO\n");
    else
        printf("condicion 1: FALSO\n");

    if (a <= b)
        printf("condicion 2: VERDADERO\n");
    else
        printf("condicion 2: FALSO\n");

    if (a > b)
        printf("condicion 3: VERDADERO\n");
    else
        printf("condicion 3: FALSO\n");

    if (a >= b)
        printf("condicion 4: VERDADERO\n");
    else
        printf("condicion 4: FALSO\n");

    if (a == b)
        printf("condicion 5: VERDADERO\n");
    else
        printf("condicion 5: FALSO\n");

    if (a != b)
        printf("condicion 6: VERDADERO\n");
    else
        printf("condicion 6: FALSO\n");

    return 0;
}
