#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//    printf("Arreglos bidimensionales: \n");
//
//    int bidimensionalArray[4][4];
//
//    bidimensionalArray[0][0] = 11;
//    bidimensionalArray[0][1] = 12;
//    bidimensionalArray[0][2] = 13;
//    bidimensionalArray[0][3] = 14;
//
//    bidimensionalArray[1][0] = 16;
//    bidimensionalArray[1][1] = 17;
//    bidimensionalArray[1][2] = 18;
//    bidimensionalArray[1][3] = 19;
//
//    bidimensionalArray[2][0] = 21;
//    bidimensionalArray[2][1] = 22;
//    bidimensionalArray[2][2] = 23;
//    bidimensionalArray[2][3] = 24;
//
//    bidimensionalArray[3][0] = 26;
//    bidimensionalArray[3][1] = 27;
//    bidimensionalArray[3][2] = 28;
//    bidimensionalArray[3][3] = 29;
//
//    printf("Valor en (0, 0): %i\n", bidimensionalArray[0][0]);
//    printf("Valor en (0, 1): %i\n", bidimensionalArray[0][1]);
//    printf("Valor en (0, 2): %i\n", bidimensionalArray[0][2]);
//    printf("Valor en (0, 3): %i\n", bidimensionalArray[0][3]);
//
//    return 0;
//}

// reto.
int main() {
    int arregloNumerico[3][4];

    arregloNumerico[0][0] = 1;
    arregloNumerico[0][1] = 1;
    arregloNumerico[0][2] = 1;
    arregloNumerico[0][3] = 1;

    int sumatoria = 0;
    for (int i=0; i < 4; i++) {
        sumatoria += arregloNumerico[0][i];
    }
    printf("La suma de la primera fila es: %i\n", sumatoria);

    arregloNumerico[1][0] = 2;
    arregloNumerico[1][1] = 2;
    arregloNumerico[1][2] = 3;
    arregloNumerico[1][3] = 3;

    sumatoria = 0;
    for (int i=0; i < 4; i++) {
        sumatoria += arregloNumerico[1][i];
    }
    printf("La suma de la segunda fila es: %i\n", sumatoria);

    arregloNumerico[2][0] = 10;
    arregloNumerico[2][1] = 0;
    arregloNumerico[2][2] = 0;
    arregloNumerico[2][3] = 16;

    sumatoria = 0;
    for (int i=0; i < 4; i++) {
        sumatoria += arregloNumerico[2][i];
    }
    printf("La suma de la tercera fila es: %i\n", sumatoria);

    return 0;
}
