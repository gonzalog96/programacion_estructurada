#include <stdio.h>
#include <stdlib.h>

// declaramos las variables globales.
//char publicText[] = {"Texto en una variable global"};
//
//void check() {
//    printf("\nImprimir desde la funcion: check\n");
//
//    printf("Variable global: ");
//    printf("%s", publicText);
//
//    printf("\nVariable local: \n");
//    //printf("%s", privateText);
//}
//
//int main()
//{
//    printf("Variables globales: \n");
//
//    char privateText[] = {"Texto en una variable local\n"};
//    printf("Variable global: ");
//    printf("%s", publicText);
//
//    printf("\nVariable local: \n");
//    printf("%s", privateText);
//
//    check();
//
//    return 0;
//}

// reto.
char nombreAlumno[50];
float calificacion;

void chequearAprobacion() {
    if (calificacion >= 7)
        printf("El alumno %s aprobo con %f\n", nombreAlumno, calificacion);
    else
        printf("El alumno %s DESAPROBO.", nombreAlumno);
}

int main() {
    printf("Ingresa el nombre del alumno: \n");
    gets(nombreAlumno);

    printf("Ingresa la calificacion del alumno: \n");
    scanf("%f", &calificacion);

    chequearAprobacion();

    return 0;
}
