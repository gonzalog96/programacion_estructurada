#include <stdio.h>
#include <stdlib.h>

int factorial(int n) {
    printf("Entra a la funcion factorial, n vale: %i\n", n);

    // caso base: n == 1.
    if (n == 1)
        return 1;
    else
        return n * factorial(n-1);
}

int main()
{
    int num, resFactorial;

    printf("Hola! Escribi un numero: \n");
    scanf("%i", &num);

    resFactorial = factorial(num);
    printf("El factorial de %i es %i\n", num, resFactorial);

    return 0;
}
