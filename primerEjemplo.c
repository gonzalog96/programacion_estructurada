#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main()
{
    int integerA;
    float floatA;
    char letterA;

    printf("Ingresa el valor del entero A: ");
    scanf("%i", &integerA);

    printf("Ingrese el valor del flotante A: ");
    scanf("%f", &floatA);

    printf("Ingrese el valor del caracter A: ");
    scanf(" %c", &letterA);

    printf("El entero A es: %i\n", integerA);
    printf("El float A es: %f\n", floatA);
    printf("El valor del car�cter A es: %c\n", letterA);

    return 0;
}
