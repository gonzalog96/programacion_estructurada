#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//    printf("Condicionales con Switch\n");
//
//    int option;
//
//    printf("Ingresa una opcion: \n");
//    scanf("%i", &option);
//
//    switch(option) {
//        case 1:
//            printf("Elegiste la opcion 1.");
//            break;
//
//        case 2:
//            printf("Elegiste la opcion 2.");
//            break;
//
//        case 3:
//            printf("Elegiste la opcion 3.");
//            break;
//
//        default:
//            printf("Elegiste una opcion invalida.");
//            break;
//    }
//
//    return 0;
//}

// reto.
int main() {
    int eleccion;

    printf("Debes elegir una de las 3 puertas: \n");
    printf("1. Gran puerta dorada \n");
    printf("2. Gran puerta roja \n");
    printf("3. Gran puerta naranja \n");
    printf("Ingresa tu eleccion: \n");
    scanf("%i", &eleccion);

    switch(eleccion) {
        case 1:
            printf("Eleccion erronea! Esta puerta te lleva al infierno \n");
            break;
        case 2:
            printf("Eleccion erronea! Esta puerta te lleva al cementerio \n");
            break;
        case 3:
            printf("Eleccion correcta! Esta puerta te lleva al camino correcto \n");
            break;
        default:
            printf("Eleccion invalida!");
            break;
    }

    return 0;
}
