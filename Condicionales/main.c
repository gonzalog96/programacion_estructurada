#include <stdio.h>
#include <stdlib.h>
#include <time.h> // para el random.

// condicionales simples.
//int main()
//{
//    printf("Condicionales\n");
//
//    float valorA, valorB, valorC;
//    valorA = valorB = 100;
//
//    printf("Primera condicion\n");
//    if (valorA == valorB)
//        printf("Ambos son iguales\n");
//
//    printf("\nSegunda condicion\n");
//    if (valorA == valorB) {
//         printf("Ambos son iguales\n");
//         valorC = valorA + valorB;
//         printf("Ademas la suma de ambos numeros es: %f\n", valorC);
//    }
//
//    return 0;
//}

// condicionales: if-else.
//int main() {
//    printf("Condicionales if, else y else if\n");
//
//    float valorA, valorB, valorC;
//    valorA = 50;
//    valorB = 100;
//    valorC = 150;
//
//    printf("Condicion: \n");
//    if (valorA == valorB)
//        printf("No se va a cumplir esta condicion\n");
//    else if (valorB == valorC)
//        printf("Tampoco se va a cumplir esta condicion\n");
//    else {
//        printf("Ninguna condicion se cumplio\n");
//        printf("Despues de esta linea el programa se cierra\n");
//    }
//
//    return 0;
//}

// reto sobre condicionales.
int main() {
    int numUsuario;
    int random = (rand() % (10 - 1 + 1)) + 1;

    printf("Ingresa un valor: \n");
    scanf("%i", &numUsuario);

    if (numUsuario == random)
        printf("Adivinaste!");
    else
        printf("Perdiste!");

    return 0;
}
