#include <stdio.h>
#include <stdlib.h>

struct personalData {
    char name[20];
    char lastName[20];

    int age;
};

void leerDatosPersona(char* ubicacion, struct personalData person) {

    FILE *archivo = fopen(ubicacion, "rb");

    if (archivo != NULL) {
        fread(&person, sizeof(person), 1, archivo);

        printf("Imprimir los datos: \n");

        printf("nombre: %s\n", person.name);
        printf("apellido: %s\n", person.lastName);
        printf("edad: %i\n", person.age);

        fclose(archivo);
    } else {
        printf("No se ha podido abrir el archivo!");
    }
}

void escribirDatos(char* ubicacion, struct personalData person) {
    FILE *archivo = fopen(ubicacion, "wb");

    if (archivo != NULL) {
        // limpiamos el buffer de input.
        fflush(stdin);

        printf("Leer datos: \n");

        printf("Ingresar el nombre: \n");
        gets(person.name);

        printf("Ingresar el apellido: \n");
        gets(person.lastName);

        printf("Ingresar la edad: \n");
        scanf("%i", &person.age);

        printf("\n---- OK!\n");

        printf("Imprimir datos: \n");
        printf("%s \n", person.name);
        printf("%s \n", person.lastName);
        printf("%i \n", person.age);

        // escribimos la info.
        fwrite(&person, sizeof(person), 1, archivo);

        // cerramos el archivo.
        fclose(archivo);
    } else {
        printf("No se ha podido crear el archivo!");
    }
}

int main()
{
    printf("Escritura y lectura de info. \n");

    struct personalData person;
    char ubicacion[] = {"datos_personales001.dat"};

    // para lectura de los datos.
    leerDatosPersona(&ubicacion, person);

    // para escritura de los datos.
    //escribirDatos(&ubicacion, person);

    return 0;
}
