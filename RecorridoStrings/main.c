#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//    printf("Cadena de caracteres \n");
//
//    char nameC[50];
//    int size;
//
//    printf("Ingresar el nombre con gets: \n");
//    gets(nameC);
//
//    printf("El nombre ingresado es: \n");
//    puts(nameC);
//
//    size = strlen(nameC);
//    printf("El tamanio de la cadena es: %i \n", size);
//
//    return 0;
//}

// reto.
int main() {
    char cadena[5];

    printf("Hola! Ingresa una palabra de 5 caracteres: \n");
    gets(cadena);

    int size = strlen(cadena);
    for (int i=size-1; i >= 0; i--) {
        printf("%c", cadena[i]);
    }

    return 0;
}
