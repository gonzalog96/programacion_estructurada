#include <stdio.h>

// calcular el factorial de 2 numeros mediante una funcion.

int factorial(n) {
  // metodo iterativo.
  for (int i=n-1; i>=1; i--) {
    n *= i;
  }
  
  return n;
}

int main() {
  int num1, num2;

  printf("Hola! Ingresa un valor: \n");
  scanf("%i", &num1);

  printf("Ahora, ingresa otro valor: \n");
  scanf("%i", &num2);

  printf("El factorial de %i es %i\n", num1, factorial(num1));
  printf("El factorial de %i es %i\n", num2, factorial(num2));


  return 0;
}