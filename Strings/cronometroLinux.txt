// hacer un cronometro que comience cuando se pulse la tecla espacio.

#include <stdio.h>
#include <unistd.h> // para sleep.

int main() {
  int h, min, seg, x;
  x = 1000;

  for (h=0; h < 24; h++) {
    for (min=0; min < 60; min++) {
      for (seg=0; seg < 60; seg++) {
        printf("%02i:%02i:%02i\n", h, min, seg);
        usleep(1000*x); //usar sleep(x) para windows
      }
    }
  }

  return 0;
}