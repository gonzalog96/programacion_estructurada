// ejemplo de memoria estatica vs pseudoestatica vs dinamica.
#include <stdio.h>
#include <stdlib.h>

// memoria estatica:
// desventajas: desaprovecha el uso de la memoria (si no se usan todos los espacios reservados) y no es posible agrandar un vector.
// ventajas: util para operaciones peque�as/testing.
//int main() {
//	int i;
//	int N=50;
//	int vector[N];
//	
//	for (i=0; i < N; i++) {
//		vector[i] = i;
//	}
//	
//	for (i=0; i < N; i++) {
//		printf("%i", vector[i]);
//	}
//	printf("\n");
//	
//	system("pause");
//	
//	return 0;
//}
//
// memoria pseudoestatica
//int main() {
//	int i, N;
//	
//	printf("Dame un valor para N: \n")
//	scanf("%i", &N);
//	
//	int vector[N];
//	
//	for (i=0; i < N; i++) {
//		vector[i] = i;
//	}
//	
//	for (i=0; i < N; i++) {
//		printf("%i", vector[i]);
//	}
//	printf("\n");
//	
//	system("pause");
//	
//	return 0;
//}

// memoria din�mica.
// ventajas: mejor empleo de la memoria en operaciones grandes/costosas.
// desventajas: mas lenta, tipado mas complicado/verbose.
int main() {
	int i, N;
	int *vector;
	
	printf("Dame otro valor para N: \n");
	scanf("%i", &N);
	
	vector = (int*)malloc(N*sizeof(int));
	if (vector == NULL) {
		printf("No se ha podido reservar memoria.\n");
	} else {
		for (i=0; i < N; i++) {
			*(vector+i) = i;
		}
		for (i=0; i < N; i++) {
			printf("%i, ", *(vector+i));
		}
		printf("\n");
		
		printf("Dame otro valor para N: \n");
		scanf("%i", &N);
		
		vector = (int*)malloc(N*sizeof(int));
		if (vector == NULL) {
			printf("No se ha podido reservar la memoria.\n");
		} else {
			for (i=0; i < N; i++) {
				*(vector+i) = i;
			}
			for (i=0; i < N; i++) {
				printf("%i, ", *(vector+i));
			}
			printf("\n");
		}
	}
	
	system("pause");
	return 0;
}