// comprobar que dos matrices dinamicas son iguales con una funcion.
#include <stdio.h>
#include <stdlib.h>

int** reservar(int filas, int columnas);
void introduce(int filas, int columnas, int**mat);
void comparar(int filas, int columnas, int **m1, int **m2);

int main() {
	int filas, columnas;
	int **m1;
	int **m2;
	
	printf("Introduce el nro. de filas: \n");
	scanf("%i", &filas);
	
	printf("Introduce el nro. de columnas: \n");
	scanf("%i", &columnas);
	
	m1 = reservar(filas, columnas);
	m2 = reservar(filas, columnas);
	
	introduce(filas, columnas, m1);
	introduce(filas, columnas, m2);
	comparar(filas, columnas, m1, m2);
		
	// liberamos la memoria.
	free(m1);
	free(m2);
		
	system("pause");
	return 0;
}

int** reservar(filas, columnas) {
	int i;
	int **mat;
	
	mat = (int**)malloc(filas*sizeof(int*));
	if (mat == NULL) {
		printf("No se ha podido reservar memoria.\n");
		exit(1);
	}
	
	for (i=0; i < filas; i++) {
		mat[i] = (int*)malloc(columnas*sizeof(int));
		if (mat[i] == NULL) {
			printf("No se ha podido reservar memoria.\n");
			exit(1);
		}
	}
	
	return mat;
}

void introduce(int filas, int columnas, int**mat) {
	int i, j;
	
	for(i=0; i < filas; i++) {
		for(j=0; j < columnas; j++) {
			printf("Introduce el valor para el elemento [%i][%i]\n", i, j);
			scanf("%i", &mat[i][j]);
		}
	}
}

void comparar(int filas, int columnas, int **m1, int **m2) {
	int i, j, aux = 0;
	
	for (i=0; i < filas && aux == 0; i++) {
		for (j=0; j < columnas && aux == 0; j++) {
			if (m1[i][j] != m2[i][j]) {
				aux = 1;
			}
		}
	}
	
	if (aux == 0) {
		printf("Ambas matrices son IGUALES\n");
	} else {
		printf("Ambas matrices NO SON IGUALES\n");
	}
}