#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//    printf("Recorrido de arreglos unidimensionales: \n");
//
//    int integerArray[11];
//    for (int i=0; i < 11; i++) {
//        integerArray[i] = i*i;
//        printf("Valor: (%i): %i\n", i, integerArray[i]);
//    }
//
//    return 0;
//}

// reto.
int main() {
    int arregloNumerico[5];

    printf("Hola! Escribi los valores: \n");

    int i = 0;
    while (i < 5) {
        printf("Valor[%i]: ", i+1);
        scanf("%i", &arregloNumerico[i]);
        i++;
    }

    // verificamos cual es el mas grande
    int mayor = arregloNumerico[0];
    for (int i=0; i < 5; i++) {
        if (arregloNumerico[i] > mayor)
            mayor = arregloNumerico[i];
    }

    printf("El numero mas grande es: %i", mayor);

    return 0;
}
