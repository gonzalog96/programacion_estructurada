#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//int suma(int a, int b) {
//    return a+b;
//}
//
//int main()
//{
//    printf("Funciones: \n");
//
//    int resultadoSuma = suma(5,5);
//    printf("%i", resultadoSuma);
//
//    return 0;
//}

// reto 1.
//int potencia(int base, int exponente) {
//    return pow(base, exponente);
//}
//
//int main() {
//    int base, exponente, resultado;
//
//    printf("Hola! Ingresa la base: \n");
//    scanf("%i", &base);
//
//    printf("Ahora, ingresa el exponente: \n");
//    scanf("%i", &exponente);
//
//    resultado = potencia(base, exponente);
//    printf("%i elevado a %i es: %i\n", base, exponente, resultado);
//
//    return 0;
//}


// reto 2.
float convertirAPesos(float dolares, float valorUnitarioEnPesos) {
    return dolares * valorUnitarioEnPesos;
}

float convertirADolares(float pesos, float valorUnitarioEnDolares) {
    return pesos/valorUnitarioEnDolares;
}

int main() {
    int opcion;
    float pesos, dolares, monto, resultado;

    printf("Hola! Ingresa '1' para cambiar de dolares a pesos; '2' para cambiar de pesos a dolares.\n");
    scanf("%i", &opcion);

    switch(opcion) {
        case 1:
            printf("Ingrese la cantidad de dolares: \n");
            scanf("%f", &monto);

            resultado = convertirAPesos(monto, 71.54);
            printf("AR$ %f - [conversion a dolares] - USD %f\n", monto, resultado);

            break;

        case 2:
            printf("Ingrese la cantidad en pesos: \n");
            scanf("%f", &monto);

            resultado = convertirADolares(monto, 71.54);
            printf("$ %f - [conversion a pesos] - USD %f\n", monto, resultado);
            break;

        default:
            printf("Opcion invalida!\n");
            break;
    }

    return 0;
}
