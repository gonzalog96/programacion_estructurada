#include <stdio.h>
#include <stdlib.h>

struct personalData {
    char name[20];
    char lastName[20];

    int age;
};

int main()
{
    printf("Structs! \n");

    struct personalData person;

    printf("Leer datos: \n");

    printf("Ingresar el nombre: \n");
    gets(person.name);

    printf("Ingresar el apellido: \n");
    gets(person.lastName);

    printf("Ingresar la edad: \n");
    scanf("%i", &person.age);

    printf("\n---- OK!\n");

    printf("Imprimir datos: \n");
    printf("%s \n", person.name);
    printf("%s \n", person.lastName);
    printf("%i \n", person.age);

    return 0;
}
