#include <stdio.h>
#include <stdlib.h>

int main()
{
    // creamos dos variables.
    int variableUno;
    int variableDos;

    // inicializamos los valores de las variables en funci�n de lo que ingrese el usuario.
    printf("�Hola! Ingres� el primer valor: ");
    scanf("%i", &variableUno);

    printf("�Hola! Ingres� el segundo valor: ");
    scanf("%i", &variableDos);

    // imprimimos el valor de las variables originales.
    printf("%i\n%i\n",variableUno, variableDos);

    // intercambiamos los valores.
    int variableAuxiliar = variableUno;

    variableUno = variableDos;
    variableDos = variableAuxiliar;

    // imprimimos los valores intercambiados.
    printf("%i\n%i", variableUno, variableDos);

    return 0;
}
