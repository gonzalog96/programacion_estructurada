float addition(float valueA, float valueB) {
    return valueA + valueB;
}

float substraction(float valueA, float valueB) {
    return valueA - valueB;
}

float multiplication(float valueA, float valueB) {
    return valueA * valueB;
}

float division(float valueA, float valueB) {
    if (valueB != 0)
        return valueA / valueB;
    return -1;
}
