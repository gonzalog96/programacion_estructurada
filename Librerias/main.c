#include <stdio.h>
#include <stdlib.h>

#include "Operaciones.h"

float solicitarValor() {
    float valor;

    printf("Ingresa un valor: \n");
    scanf("%f", &valor);

    return valor;
}

void imprimirResultado(int tipoOperacion) {
    float firstValue =  solicitarValor();
    float secondValue = solicitarValor();
    float result;

    // 1 para suma; 2 para resta; 3 para producto y 4 para division.
    switch(tipoOperacion) {
        case 1:
            result = addition(firstValue, secondValue);
            printf("El resultado de la suma entre %f y %f es: %f", firstValue, secondValue, result);
            break;

        case 2:
            result = substraction(firstValue, secondValue);
            printf("El resultado de la diferencia entre %f y %f es: %f", firstValue, secondValue, result);
            break;

        case 3:
            result = multiplication(firstValue, secondValue);
            printf("El resultado del producto entre %f y %f es: %f", firstValue, secondValue, result);
            break;

        case 4:
            result = division(firstValue, secondValue);
            printf("El resultado de la division entre %f y %f es: %f", firstValue, secondValue, result);
            break;
    }
}

int main()
{
    printf("Librerias!\n");

    int option;

    printf("Soy una calculadora! \n");
    printf("Que operacion deseas realizar?\n");
    printf("--\n");
    printf("1. Suma\n");
    printf("2. Resta\n");
    printf("3. Multiplicacion\n");
    printf("4. Division\n");
    printf("--\n");
    printf("Elegir alguna de estas opciones\n");

    scanf("%i", &option);

    switch(option) {
        case 1:
            imprimirResultado(1);
            break;
        case 2:
            imprimirResultado(2);
            break;
        case 3:
            imprimirResultado(3);
            break;
        case 4:
            imprimirResultado(4);
            break;
        default:
            printf("Error! Opcion invalida.");
            break;
    }

    return 0;
}
