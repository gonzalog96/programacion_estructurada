#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//    printf("Arreglos unidimensionales: \n");
//
//    int integerList[3];
//    integerList[0] = 4;
//    integerList[1] = 5;
//    integerList[2] = 6;
//
//    float floatList[] = {23.4, 34.5, 45.6, 21.8, 56.7};
//
//    char charList[4];
//    charList[0] = 'G';
//    charList[1] = 'O';
//    charList[2] = 'N';
//    charList[3] = 'Z';
//    charList[4] = 'A';
//
//    printf("\n Primero entero: \t\t %i\n", integerList[0]);
//    printf("\n Ultimo flotante: \t\t %f", floatList[4]);
//    printf("\n Lista de caracteres: \t\t %c%c%c%c%c. \n", charList[0], charList[1], charList[2], charList[3], charList[4]);
//
//    return 0;
//}


// reto.
int main() {
    int listaNumeros[5];

    int i = 0;
    do {
        printf("Hola! Ingresa el elemento %i del arreglo\n", i+1);
        scanf("%i", &listaNumeros[i]);

        i++;
    } while (i < 5);

    int producto = 1;
    int length = sizeof(listaNumeros) / sizeof(int);
    for (int i=0; i < length; i++) {
        producto *= listaNumeros[i];
    }

    printf("El producto de todos los numeros de la lista es: %i", producto);

    return 0;
}
